Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kaddressbook
Upstream-Contact: kde-devel@kde.org
Source: https://invent.kde.org/pim/kaddressbook

Files: *
Copyright: 2020, Carson Black <uhhadd@gmail.com>
           2000-2004, Cornelius Schumacher <schumacher@kde.org>
           2020, Konrad Czapla <kondzio89dev@gmail.com>
           2009-2024, Laurent Montel <montel@kde.org>
           1998-1999, Preston Brown <pbrown@kde.org>
           2007-2009, Tobias Koenig <tokoe@kde.org>
License: GPL-2+

Files: src/activities/accountactivities.cpp
       src/activities/accountactivities.h
       src/activities/activitiesmanager.cpp
       src/activities/activitiesmanager.h
       src/activities/autotests/activitiesmanagertest.cpp
       src/activities/autotests/activitiesmanagertest.h
       src/activities/ldapactivities.cpp
       src/activities/ldapactivities.h
Copyright: 2024, Laurent Montel <montel@kde.org>
License: GPL-2

Files: src/printing/compact/compactstyle.cpp
       src/printing/compact/compactstyle.h
       src/printing/detailled/detailledstyle.cpp
       src/printing/detailled/detailledstyle.h
       src/printing/mike/mikesstyle.cpp
       src/printing/mike/mikesstyle.h
       src/printing/printingwizard.cpp
       src/printing/printingwizard.h
       src/printing/printprogress.cpp
       src/printing/printprogress.h
       src/printing/printstyle.cpp
       src/printing/printstyle.h
       src/printing/ringbinder/ringbinderstyle.cpp
       src/printing/ringbinder/ringbinderstyle.h
       src/printing/stylepage.cpp
       src/printing/stylepage.h
Copyright: 2002, Anders Lund <anders.lund@lund.tdcadsl.dk>
           2002, Jost Schenck <jost@schenck.de>
           2009-2024, Laurent Montel <montel@kde.org>
           2011, Mario Scheel <zweistein12@gmx.de>
           2002, Mike Pilone <mpilone@slac.com>
           1996-2002, Mirko Boehm <mirko@kde.org>
           2009, Tobias Koenig <tokoe@kde.org>
           Tobias Koenig <tokoe@kde.org>
License: GPL-2+_WITH_Qt-Commercial-exception-1.0

Files: src/categoryfilterproxymodel.cpp
       src/categoryfilterproxymodel.h
       src/categoryselectwidget.cpp
       src/categoryselectwidget.h
       src/contactswitcher.cpp
       src/contactswitcher.h
       src/importexport/contactselectiondialog.cpp
       src/importexport/contactselectiondialog.h
       src/importexport/contactselectionwidget.cpp
       src/importexport/contactselectionwidget.h
       src/stylecontactlistdelegate.cpp
       src/uistatesaver.cpp
       src/uistatesaver.h
       src/widgets/quicksearchwidget.cpp
       src/widgets/quicksearchwidget.h
Copyright: 2020, Carson Black <uhhadd@gmail.com>
           2014, Jonathan Marten <jjm@keelhaul.me.uk>
           2020, Konrad Czapla <kondzio89dev@gmail.com>
           2015-2024, Laurent Montel <montel@kde.org>
           2009, Tobias Koenig <tokoe@kde.org>
           2008, Volker Krause <vkrause@kde.org>
License: LGPL-2+

Files: CMakePresets.json
       src/activities/autotests/CMakeLists.txt
Copyright: 2021-2024, Laurent Montel <montel@kde.org>
License: BSD-3-Clause

Files: CMakeLists.txt
       doc/CMakeLists.txt
       .git-blame-ignore-revs
       .gitignore
       .gitlab-ci.yml
       .kde-ci.yml
       kontactplugin/CMakeLists.txt
       .krazy
       sanitizers.supp
       src/CMakeLists.txt
       src/data/org.kde.kaddressbook.appdata.xml
Copyright: 2020-2024, Laurent Montel <montel@kde.org>
           None
License: CC0-1.0

Files: po/uk/*
Copyright: 2009-2018, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

Files: debian/*
Copyright: 2009, Sune Vuorela <debian@pusling.com>
           2024 Patrick Franz <deltaone@debian.org>
License: LGPL-2+

License: CC0-1.0
 CC0 1.0 Universal
 .
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
 LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
 ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
 INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
 REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
 THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
 HEREUNDER.
 .
 The complete text of the Creative Commons 0 1.0 Universal
 can be found in `/usr/share/common-licenses/CC0-1.0'.

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the license.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 License for more details.
 .
 The complete text of the GNU Library General Public License version 2
 can be found in `/usr/share/common-licenses/LGPL-2'.

License: GPL-2+_WITH_Qt-Commercial-exception-1.0
 The complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.
 .
 As a special exception, the copyright holder(s) give permission to link
 this program with the Qt Library (commercial or non-commercial edition),
 and distribute the resulting executable, without including the source
 code for the Qt library in the source distribution.

License: LGPL-2.1+3+KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 The complete text of the GNU Library General Public License version 2
 can be found in `/usr/share/common-licenses/LGPL-2', likewise,
 the complete text of the GNU Lesser General Public License version 3
 can be found in `/usr/share/common-licenses/LGPL-3'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
